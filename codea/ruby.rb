def fact_rec(num)
  if num <= 1
    num
  else
    num + fact(num-1)
  end
end

def fact(num)
  (0..num).reduce(&:+)
end

p fact 50000000
p fact_rec 50000000
