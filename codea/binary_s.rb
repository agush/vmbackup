def binary_search(arr, a, min=nil, max=nil)
  return -1 if arr.empty?
  min ||= 0
  max ||= arr.count-1
  mid = (min-max)/2
  if a = arr[mid]
    return mid
  elsif a < arr[mid]
    binary_search(arr, a, min, mid)
  elsif a > arr[mid]
    binary_search(arr, a, mid, max)
  end
end

puts binary_search([1, 3, 4, 6, 8, 9, 11],4) ==2
puts binary_search([1, 4, 6, 7],4)==1
puts binary_search([2,4,6,8,10],10)
