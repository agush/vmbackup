require 'minitest/autorun'
require 'minitest/pride'

def flatten(array)
  array.to_s.gsub!(/[\[\]]/,"").split(",").map(&:to_i)
end


class FlattenTest < Minitest::Test

  def test_single
    assert_equal [1,2,3,4,5,6], flatten([1,2,3,4,5,6])
  end

  def test_single
    assert_equal [1,2,3,4,5,6], flatten([[1,2],3,4,[5,6]])
  end

  def test_double_nest
    assert_equal [1,2,3,4,5,6], flatten([1,2,[3,4,[5,6]]])
  end

  def test_triple_nest
    assert_equal [1,2,3,4,5,6], flatten([1,[2,[3,4,[5,6]]]])
  end

  def test_citrusbyte
    assert_equal [1,2,3,4], flatten([[1,2,[3]],4])
  end
end
