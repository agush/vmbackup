require 'minitest/autorun'
require 'minitest/pride'

def flatten(array, res = [])
  array.each do |el|
    if el.kind_of? Array
      flatten(el, res)
    else
      res << el
    end
  end
  res
end


class FlattenTest < Minitest::Test

  def test_single
    assert_equal [1,2,3,4,5,6], flatten([1,2,3,4,5,6])
  end

  def test_single
    assert_equal [1,2,3,4,5,6], flatten([[1,2],3,4,[5,6]])
  end

  def test_double_nest
    assert_equal [1,2,3,4,5,6], flatten([1,2,[3,4,[5,6]]])
  end

  def test_triple_nest
    assert_equal [1,2,3,4,5,6], flatten([1,[2,[3,4,[5,6]]]])
  end

  def test_citrusbyte
    assert_equal [1,2,3,4], flatten([[1,2,[3]],4])
  end
end
