require 'benchmark'

def arr_flatten(array, res = [])
  array.each do |el|
    if el.kind_of? Array
      arr_flatten(el, res)
    else
      res << el
    end
  end
  res
end

def str_flatten(array)
  array.to_s.gsub!(/[\[\]]/,"").split(",").map(&:to_i)
end

def random_value
  if rand(3) < 1
    (rand(5)+1).times.map { |i| random_value }
  else
    rand(10000)
  end
end

VALUE = 100.times.map { |i| random_value}
ITERATIONS = 1000

Benchmark.bm do |b|
  puts "Iterations: #{ITERATIONS}"
  b.report("Arr version") do
    ITERATIONS.times { |i| arr_flatten(VALUE)}
  end
  b.report("Str version") do
    ITERATIONS.times { |i| str_flatten(VALUE)}
  end
end
