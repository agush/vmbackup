class Furlong
  KM_PER_MILE = 1.60934
  def miles_to_km(miles)
    miles * KM_PER_MILE
  end
end
