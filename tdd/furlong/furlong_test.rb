require 'minitest/autorun'
require 'minitest/pride'
require './furlong'

class FurlongTest < Minitest::Unit::TestCase
  def test_one_mile
    km = Furlong.new.miles_to_km(1)
    assert_in_delta 1.60934, km, 0.001
  end
end
